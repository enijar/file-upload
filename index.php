<?php
	$page_title = 'Secure File Uploader';

	require_once 'header.php';

    $files = get_all_files();
?>

        <div id="file-uploader">
            Upload Files
        </div>

        <div id="uploaded-files-preview" class="clearfix"></div>

        <?php if($files): ?>
            <?php foreach($files as $file): ?>

                <h3>File type: <?php echo $file['type']; ?></h3>
                <img src="<?php echo $file['path']; ?>" width="250">

            <?php endforeach; ?>
        <?php endif; ?>

<?php
	require_once 'footer.php';
?>