<?php
    require_once 'functions.php';

    $file = isset($_FILES['file']) ? $_FILES['file'] : false;
    $upload_path = dirname(__FILE__) . '/uploads/';

    if($file) {
        upload_file($file, $upload_path, $config);
    }