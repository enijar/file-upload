<?php
	session_start();
	
	require_once 'functions.php';
?>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no">
        <meta name="description" content="">
	
		<title><?php echo $page_title; ?></title>
		<link rel="stylesheet" href="<?php echo $config['site']['root']; ?>/css/global.css">
		<script src="<?php echo $config['site']['root']; ?>/js/jquery.min.js"></script>
        <script src="<?php echo $config['site']['root']; ?>/js/dropzone.min.js"></script>
        <script src="<?php echo $config['site']['root']; ?>/js/global.js"></script>
	</head>
	
	<body>
	
		<div id="wrapper">
			<div id="container" class="clearfix">