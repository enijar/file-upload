$(function() {
    // Dropzone js ajax file upload
    $('#file-uploader').dropzone({
        url: 'upload.php',
        maxFilesize: 3072,
        maxFiles: 4,
        // Uploaded files container
        previewsContainer: '#uploaded-files-preview',
        // Allows specific MIME type to be selected for upload
        acceptedFiles: 'image/*',
        // File dropped into upload area
        drop: function() {
            console.log('Something was dropped into the upload area.');
        },
        // File dragged over upload area
        dragover: function() {
            console.log('Something is being dragged over the upload area.');
        },
        // Shows upload progress (0-100)
        uploadprogress: function(file, progress) {
            $('.dz-upload').css('width', progress);
            console.log(progress);
        },
        // Send file and form data to server
        sending: function(file, xhr) {
            console.log(file);
            console.log(xhr);
        },
        // Upload complete and response from server
        complete: function(file, response) {
            console.log('Upload complete');

            if(response) {
                console.log(response);
            }
        }
    });
});