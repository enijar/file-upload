<?php
	require_once 'config.php';

    function get_all_files()
    {
        global $db;

        $stmt = $db->prepare("
            SELECT path, type
            FROM file_paths
        ");
        $stmt->execute();
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }

    function store_file_path($file, $type)
    {
        global $db;

        $stmt = $db->prepare("
            INSERT INTO file_paths
            (path, type)
            VALUES(?, ?)
        ");
        $stmt->execute(array($file, $type));
    }

    function upload_file($file, $upload_path, $config)
    {
        // New (more secure code)
        $file_info = finfo_open(FILEINFO_MIME_TYPE);
        $mime_type = finfo_file($file_info, $file['tmp_name']);
        $mime_parts = explode('/', $mime_type);
        $mime_type = $mime_parts[0];
        $image_type = $mime_parts[1];

        if($mime_type === 'image') {
            $upload_name = time() . '-' . hash('sha512', $file['name']) . '.' . $image_type;
            $upload_path = $upload_path . $upload_name;

            move_uploaded_file($file['tmp_name'], $upload_path);
            store_file_path($config['site']['uploads'] . '/' . $upload_name, $mime_type);
        } else {
            return false;
        }

        finfo_close($file_info);
    }